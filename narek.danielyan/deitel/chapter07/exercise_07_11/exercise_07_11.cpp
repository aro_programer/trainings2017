#include<iostream>

int
main() 
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE] = {0};

    for (int l = 0; l < ARRAY_SIZE; ++l) {
        std::cin >> array[l];
    }
    for (int i = 0; i < ARRAY_SIZE; ++i) {
        for (int j = 0; j < ARRAY_SIZE - 1; ++j) {
            if (array[j] > array[j + 1]) {
                int tmp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = tmp;
            }
        }
    }
    for (int k = 0; k < ARRAY_SIZE; ++k) {
        std::cout << array[k] << " " << std::endl;
    }
    return 0;
}

