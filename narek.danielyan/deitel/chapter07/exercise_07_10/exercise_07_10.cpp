#include<iostream>

int
main() 
{
    const int FREQUENCY_SIZE = 9;
    int wageFrequency[FREQUENCY_SIZE] = {0};

    while (true) {
        int weeklySelling;
        std::cout << "Input weekly selling or -1 to exit: ";
        std::cin >> weeklySelling;
        if (-1 == weeklySelling) {
            std::cout << std::endl;
            break;
        } 
        int wage = ((weeklySelling * 9) / 100 + 200); 
        ++wageFrequency[wage / 100 - 2];
        std::cout << std::endl;
    }

    int count;
    for (count = 0; count < FREQUENCY_SIZE - 1; ++count) {
        const int wageSizeFrom = (count + 2) * 100;
        const int wageSizeTo = wageSizeFrom + 99;
        std::cout << wageSizeFrom << "$" << "-" << wageSizeTo << "$: ";
        std::cout << wageFrequency[count] << std::endl;
    }
    std::cout << "1000$ and more: ";
    std::cout << wageFrequency[count] << std::endl;
    return 0;
}

