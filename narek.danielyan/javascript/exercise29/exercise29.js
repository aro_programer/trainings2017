$(document).ready(function () {
    "use strict";
    var arr = [], number = " ", result = 0;
    while (true) {
        number = prompt("input number");
        if (number === null || number == "" || isNaN(number)) {
            alert("Inputting is not number");
            break;
        }
        arr.push(+number);
    }
    result = arr.reduce(function (sum, current) {
        return sum + current;
    }, 0);
    $("#sum").text(result);
});
