It's like a binary search. The number that lies in the range from 1 to 1000 can be guessed only 10 times, 
because 1000 can be divided into only 10 ranges by division on two, where our estimated number can be found.

1000 / 2 = 500                             1
500 / 2 = 250                              2 
250 / 2 = 125                              3
125 / 2 = 62.5                             4
62.5 / 2 = 31.25                           5
31.25 / 2 = 15.626                         6
15.625 / 2 = 7.8125                        7 
7.8125 / 2 = 3.90625                       8
3.90625 / 2 = 1.953125                     9
1.953125 / 2 = 0.9765625 - its mean 1      10
