#include <iostream>

int numberInversion(const int number);

int 
main()
{
    int number;
    std::cin >> number;
    std::cout << numberInversion(number) << std::endl;

    return 0;
}

int
numberInversion(const int number)
{
    int numberCopy = number;
    int degree = 1;
    do {
        numberCopy /= 10;
        degree *= 10;
    } while (numberCopy != 0);

    degree /= 10;
    numberCopy = number;
    int invertedNumber = 0;
    do {
        invertedNumber += numberCopy % 10 * degree;
        numberCopy /= 10;
        degree /= 10;
    } while (numberCopy != 0);

    return invertedNumber;
}


