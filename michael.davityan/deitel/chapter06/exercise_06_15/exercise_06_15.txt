Question. Answer each of the following questions:
a. What does it mean to choose numbers "at random?"
b. Why is the rand function useful for simulating games of chance?
c. Why would you randomize a program by using srand? Under what circumstances is it desirable not to 
randomize?
d. Why is it often necessary to scale or shift the values produced by rand ?
e. Why is computerized simulation of real-world situations a useful technique?

Answer.
a. It means!!!, for example i dont now games of chance that gamers play in casino but i now that they use in 
their games role dice or casino roulette. When a player throws a dice, he does not know what will fall out of the numbers(from 1 tо 6). For this purpose to simulate a real game of casinos on PC we use in C++ randomization 
function(rand()), that generate a random whole number from 0 to RAND_MAX(32767), it can help to generate a random number of role dices when player throw, or roulette stop number when player turns it.

b.because rand function generate a random whole number from 0 to RAND_MAX(32767). For example when we want to 
generate a random numbers of role dices we can write this 1 + rand() % 6 that give us a random number 
from 1 to 6.

c.for example when we use rand function in program to generate а set of numbers, after the end of program when we execute program the other time again, its generate the same set of numbers like the last time. This is not 
desirable. That is why we use srand() function. srand() takes an unsignet integer number like argument that 
rand() use to generate a different set of random whole numbers when program executes again and again in different times.

d.when rand() function works it generates a random numbers from 0 to 32767, for example when we want to have a set of random numbers from 1 to 6 for role dices we can use random function like this rand() % 6, 
because every whole number after modulo 6 gives the remainder from 0 to 5, and if we want to have a set of number from 1 to 6 we can do this!!!! rand() % 6 + 1.

e. Because we can use it in some spheres of our live, for example where we dont need a real tank to learn how use a tank. The tank simulator that works on the same physics like the real tank allows us to learn virtualy tank 
driving that gives us a real tank driving experience. And many other spheres in our live can use computerized 
simulation of real-world situations. Its very effective.
