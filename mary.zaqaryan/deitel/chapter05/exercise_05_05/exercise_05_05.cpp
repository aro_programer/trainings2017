#include <iostream>

int 
main()
{
    int amount;
    std::cout << "Enter the amount of numbers: ";
    std::cin >> amount;

    if (amount < 0) {
        std::cerr << "Error 1: amount can't be negative or equal 0.";
        return 1;
    }

    int sum = 0;
    
    for (int i = 1; i <= amount; ++i) {
        int number;
        std::cout << "Enter number: ";
        std::cin >> number;
        sum += number;
    }

    std::cout << "Sequence of integers is " << sum << std::endl;

    return 0;
}
