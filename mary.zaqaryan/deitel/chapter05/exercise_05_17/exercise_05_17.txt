i = 1;
j = 2;
k = 3; 
m = 2;

a) std::cout << (i == 1) << std::endl; 
   answer: 1 (with brackets)

b) std::cout << (j == 3) << std::endl; 
   answer: 0 (with brackets)

c) std::cout << (i >=1 && j < 4) << std::endl; 
   answer: 1 (with brackets)

d) std::cout << (m <= 99 && k < m) << std::endl; 
   answer: 0 (with brackets)
   
e) std::cout << (j >= i || k == m) << std::endl; 
   answer: 1 (with brackets)
   
f) std::cout << (k + m < j || 3 - j >= k) << std::endl;
   answer: 0 (with brackets)
   
g) std::cout << (!m) << std::endl;
   answer: 0 (without brackets)
   
h) std::cout << (!(j - m)) << std::endl;
   answer: 1 (without outside brackets)
   
i) std::cout << (!(k > m)) << std::endl;
   answer: 0 (without outside brackets)
