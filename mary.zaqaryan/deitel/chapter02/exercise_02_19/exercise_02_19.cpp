#include <iostream>

int
main()
{
    std::cout << "Input three different integers: ";
    int number1;
    std::cin >> number1;
    int number2;
    std::cin >> number2;
    int number3;
    std::cin >> number3;

    int sum = number1 + number2 + number3;
    std::cout << "Sum is " << sum << "\n";

    int average = sum / 3;
    std::cout << "Average is " << average << "\n";

    int product = number1 * number2 * number3;
    std::cout << "Product is " << product << "\n";

    int min = number1;

    if (number2 < min) {
        min = number2;
    }

    if(number3 < min) {
        min = number3;
    };

    int max = number1;
    
    if (number2 > max) {
        max = number2;
    }

    if (number3 > max) {
        max = number3;
    }

    std::cout << "Smallest is " << min << "\n";
    std::cout << "Largest is " << max <<"\n";
    
    return 0;
}

