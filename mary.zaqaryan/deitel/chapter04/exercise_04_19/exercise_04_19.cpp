#include <iostream>
#include <climits>

int 
main()
{
    int counter = 1;
    int max = INT_MIN;
    int secondMax = INT_MIN;
    std::cout << "Enter 10 numbers\n";
    
    while (counter <= 10) {
        int number;
        std::cin >> number;

        if (number > max) {
            secondMax = max;
            max = number;
        } else if (number > secondMax) {
            secondMax = number;
        }

        ++counter;
    }
    std::cout << "The largest is: " << max << std::endl;
    std::cout << "The second largest is: " << secondMax << std::endl;
    return 0;
    return 0;
}
