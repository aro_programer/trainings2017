#include <iostream>

int
main()
{
    while (true) {
        double sales;
        std::cout << "Enter sales in dollars (-1 to end): ";
        std::cin >> sales;
        if (-1 == sales) {
            break;
        }
    
        double salary = 200 + (sales * 9 / 100);
        std::cout << "Salary is: $" << salary << std::endl;
    }
    return 0;
}
