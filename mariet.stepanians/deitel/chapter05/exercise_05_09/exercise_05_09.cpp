#include <iostream>

int
main() 
{
    int product = 1;
    for (int number = 1; number <= 15; number += 2) {
        product *= number;
    }
    std::cout << "The product of the odd integers from 1 to 15 --> " << product << std::endl;
    
    return 0;    
}
