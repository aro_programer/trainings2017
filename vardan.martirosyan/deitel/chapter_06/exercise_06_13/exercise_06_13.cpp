#include <iostream>
#include <cmath>

int
main()
{
    for (int i = 1; i <= 3; ++i) {
        float number;
        std::cout << "Enter number: ";
        std::cin  >> number;
        if(std::cin.fail()) {
            std::cout << "Error 1: Invalid input, your input is not a digit. " << std::endl;
            return 1;
        }
        std::cout << "Entered number is " << number << "\nChanged number is " << std::floor(number + 0.5) << std::endl;
    }
    return 0;
}

