#include <iostream>
#include <cassert>

bool
multiple(const int numberOne, const int numberTwo)
{
    assert(numberTwo != 0);
    return 0 == numberOne % numberTwo;
}

int
main()
{
    int numberOne;
    std::cout << "Enter first number: ";
    std::cin >> numberOne;
    int numberTwo;
    std::cout << "Enter second number: ";
    std::cin >> numberTwo;
    if (0 == numberTwo) {
        std::cerr << "Error 1: Number can not be zero. " << std::endl;
        return 1;
    }
    if (multiple(numberOne, numberTwo)) {
        std::cout << "Second number is a multiple of the first number.\n";
    } else {
        std::cout << "Second number not multiple of the first number.\n";
    }
    return 0;
}
