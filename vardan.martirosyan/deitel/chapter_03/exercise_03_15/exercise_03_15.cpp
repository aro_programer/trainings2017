#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date1(1, 1, 1);

    int day;
    std::cout << "Enter the day. It can be from 1 to 31: ";
    std::cin  >> day;
    date1.setDay(day);

    int month;
    std::cout << "Enter the month. It can be from 1 to 12: ";
    std::cin  >> month;
    date1.setMonth(month);

    int year;
    std::cout << "Enter year: ";
    std::cin  >> year;
    date1.setYear(year);

    date1.displayDate();

    return 0;
}

