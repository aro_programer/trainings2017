#include <string>

class Invoice 
{
public:
    Invoice(std::string number, std::string description, int quantity, int price);
    void setNumber(std::string number);
    std::string getNumber();
    void setDescription(std::string description);
    std::string getDescription();
    void setQuantity(int quantity);
    int getQuantity();
    void setPrice(int pricePerItem);
    int getPrice();
    int getInvoiceAmount();

private:
    std::string number_;
    std::string description_;
    int quantity_;
    int price_;
};
