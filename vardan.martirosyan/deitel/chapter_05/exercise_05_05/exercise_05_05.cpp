#include <iostream>

int
main()
{
    int count;
    std::cout << "Enter count: ";
    std::cin  >> count;

    if (count < 1) {
        std::cerr << "Error 1: Numeric can not be negative." << std::endl;
        return 1;
    }

    int sum = 0;
    for (int i = 1; i <= count; ++i) {
        int number;
        std::cout << "Enter number: ";
        std::cin  >> number;
        sum += number;
    }
    std::cout << "Sum is " << sum << std::endl;
    
    return 0;
}

