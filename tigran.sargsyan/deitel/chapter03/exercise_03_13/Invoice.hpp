#include <iostream>
#include <string>

class Invoice
{
public:
    Invoice(std::string partNumber, std::string description, int quantity, int price);
    void setPartNumber(std::string partNumber);
    std::string getPartNumber();
    void setDescription(std::string description);
    std::string getDescription();
    void setQuantity(int quantity);
    int getQuantity();
    void setPrice(int price);
    int getPrice();
    int getInvoiceAmount();

private:
    std::string partNumber_;
    std::string description_;
    int quantity_;
    int price_;
};

