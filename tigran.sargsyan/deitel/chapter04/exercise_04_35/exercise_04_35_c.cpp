#include <iostream>

int
main()
{
    int accuracy;
    std::cout << "Enter accuracy: ";
    std::cin >> accuracy;

    if (accuracy < 0) {
        std::cerr << "Error 1. Entered number should be non-negative." << std::endl;
        return 1;
    }

    double power;
    std::cout << "Enter power: ";
    std::cin >> power;

    int currentAccuracy = 0;
    double exponent = 0, factorial = 1, currentPower = 1;
    while (currentAccuracy <= accuracy) {
        exponent += (currentPower / factorial);
        currentPower *= power;
        ++currentAccuracy;
	factorial *= currentAccuracy;
    }

    std::cout << "Exponent value: " << exponent << std::endl;
    return 0;
}

