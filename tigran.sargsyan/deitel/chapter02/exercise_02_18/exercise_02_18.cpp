#include <iostream>

int
main()
{
    int number1, number2;

    std::cout << "Enter two numbers: ";
    std::cin >> number1 >> number2;

    if (number1 > number2) {
        std::cout << number1 << " is greater." << std::endl;
    }

    if (number1 < number2) {
        std::cout << number2 << " is greater." << std::endl;
    }

    if (number1 == number2) {
        std::cout << "Entered numbers are equal." << std::endl;
    }

    return 0;
}

