A)

In the function prototype we said, that function
returns float, but in declaration we said, that
it returns double. That is wrong. Correct code 
will be lie this - 

float cube(float number);

float 
cube(float number) {
    return number * number * number; 
}


B)

Type can't have 2 memory specifiers. Right code 
will be like this -

register int x = 7;


C)

We can't assign srand() to variable. Correct code 
will be like this - 

int randomNumber = rand();


D)

y is float, and x is int. We can't assign y to x.
Right code will be like this - 

float y = 123.45678;
float x = y;

std::cout << x << std::endl


E)

We already have attribute called number. Declaring
a double variable named number is wrong, because we
can use function's attribute. Correct code will be 
like this - 

double 
square(const double number) {
    return number * number;
}

F)

In function body we must call function sum with 
n - 1 argument. This is endless program. Right code
will be like this -

int
sum(const int n) {
    if (0 == n) {
        return 0;
    } else {
        return n + sum(n - 1);
    }
}
