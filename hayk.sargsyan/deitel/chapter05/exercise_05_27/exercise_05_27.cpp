#include <iostream>

int
main()
{
    for (int counter = 1; counter <= 10; ++counter) {
        if (counter != 5) {
            std::cout << " ";
        }
    }

    std::cout << std::endl 
              << "Used continue to skip printing 5"
              << std::endl;

    return 0;
}
