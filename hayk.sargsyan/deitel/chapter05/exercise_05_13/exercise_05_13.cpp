#include <iostream>

int
main()
{
    for (int repeat = 1; repeat <= 5; ++repeat) {
        int printNumber;
        std::cout << "Please enter your number (1-30): ";
        std::cin >> printNumber;
        if (printNumber < 1 || printNumber > 30) {
            std::cerr << "Error 1: Invalid Number" << std::endl;
            return 1;
        }
        for (int counter = 1; counter <= printNumber; ++counter) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    return 0;
}
