#include <iostream>

int
main()
{
    char character;
    std::cout << "Enter a character: ";
    std::cin >> character;
    std::cout << static_cast<int>(character) << std::endl;
    return 0;
}

