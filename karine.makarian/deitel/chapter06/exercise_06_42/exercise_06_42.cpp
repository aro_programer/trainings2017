#include <iostream>
#include <cassert>

void
hanoi(const int count, const int start, const int temp, const int end)
{
    assert(count > 0 && start > 0 && temp > 0 && end > 0);
    assert(start != end);
    if (1 == count) {
        std::cout << start << "-->" << end << std::endl;
    } else {
        hanoi(count -1, start, end, temp);
        hanoi(1, start, temp, end);
        hanoi(count - 1, temp, start, end);
    }
}

int
main()
{
    int count;
    std::cout << "Enter the count of disks: ";
    std::cin >> count;

    if (count <= 0) {
        std::cerr << "Error 1: Invalid count." << std::endl;
        return 1;
    }

    int start;
    int end;
    
    std::cout << "Enter the numbers for start, end pegs (from 1 to 3): ";
    std::cin >> start >> end;
    
    if (start == end) {
        std::cerr << "Error 2: Numbers for start and end pegs can't be the same." << std::endl;
        return 2;
    }
    if ((start < 1 && start > 3) || (end < 1 && end > 3)) {
        std::cerr << "Error 3: Invalid value for pegs." << std::endl;
        return 3;
    }

    const int temp = 6 - (start + end);

    hanoi(count, start, temp, end);

    return 0;
}
