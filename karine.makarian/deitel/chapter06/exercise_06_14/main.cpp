#include <iostream>
#include <cmath>

double
roundToInteger(const double number)
{
    const double calculation = std::floor(number * 10 + 0.5) / 10;

    return calculation;
}

double
roundToTenths(const double number)
{
   const double calculation = std::floor(number * 100 + 0.5) / 100;

    return calculation;
}

double
roundToHundreths(const double number)
{
   const double calculation = std::floor(number * 1000 + 0.5) / 1000;

    return calculation;
}

double
roundToThousanths(const double number)
{
    const double calculation = std::floor(number * 10000 + 0.5) / 10000;

    return calculation;
}

int
main()
{
    const double roundToInt = roundToInteger(10.1234);
    const double roundToTen = roundToTenths(10.1234);
    const double roundToHundred = roundToHundreths(10.1234);
    const double roundToThousand = roundToThousanths(10.1234);

    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToInt << std::endl;
    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToTen << std::endl;
    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToHundred << std::endl;
    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToThousand << std::endl;
        
    return 0;
}
