#include <iostream>

int
main()
{
    static int count = 1;
    std::cout << "Calling main fuction recursively. Count's value is " << count << std::endl;

    count++;
    main();
   
    return 0;
}
///main() function is called recursively, count's value is different each time the program is running 
///and core dumpes, which means the stack, that has 1 mb size, is already full.
