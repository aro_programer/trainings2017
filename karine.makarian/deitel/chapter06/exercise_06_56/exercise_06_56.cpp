#include <iostream>
#include <unistd.h>

int tripleCallByValue(int number);

void tripleByReference(int &number);

int
main()
{
    int number; 
    std::cin >> number;
 
    std::cout << "Number's value before call by value: " << number << std::endl;
    std::cout << "After tripleCallByValue function call: " 
              << tripleCallByValue(number) << std::endl;
    std::cout << "Number's value after  call by value: " << number << std::endl;

    std::cout << "Number's value before call by reference: " << number << std::endl;
    tripleByReference(number);
    std::cout << "Number's value after  call by reference: " << number << std::endl;

    return 0;
}

int 
tripleCallByValue(int number)
{
    number *= 3;
    return number;
}

void 
tripleByReference(int &number)
{
    number *= 3;
}

