#include <iostream>
#include <cassert>

int 
quotient (const int number1, const int number2)
{
    assert(number2 != 0);
    return number1 / number2;
}

int 
remainder (int number1, int number2)
{
    assert(number2 != 0);
    return number1 % number2;
}

void
print(const int number) 
{
    assert(number > 0 && number < 32768);
    int size = 10;
    int copy = number;

    while (quotient(copy, size) > 0) {
        size *= 10;
    }

    size = quotient(size, 10);

    while (size != 0) {
        std::cout << quotient(copy, size) << " ";
        copy = remainder(copy, size);
        size = quotient(size, 10);
    }

    std::cout << std::endl;
}

int
main()
{
    int number;
    std::cout << "Enter a number (from 1 to 32767): ";
    std::cin >> number;

    if (number < 1 || number > 32767) {
        std::cerr << "Error 1: Invalid number." << std::endl;
        return 1;
    }

    print(number);    
    return 0;
}

