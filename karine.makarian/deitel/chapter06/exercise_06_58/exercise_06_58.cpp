#include <iostream>

template < typename K > 
K
minimum( const K value1, const K value2 )
{
    return (value1 > value2) ? value2 : value1;
}

int 
main()
{
    int int1, int2;
    std::cout << "Input two integer values: ";
    std::cin >> int1 >> int2;
 
    std::cout << "The minimum integer value is: "
              << minimum( int1, int2 ) << std::endl;
        
    double double1, double2;
    std::cout << "Input two double values: ";
    std::cin >> double1 >> double2;
    
    std::cout << "The minimum double value is: "
              << minimum( double1, double2 ) << std::endl;
             
    char char1, char2;
    std::cout << "Input two characters: ";
    std::cin >> char1 >> char2;
         
    std::cout << "The minimum character value is: "
             << minimum( char1, char2 ) << std::endl;
    
    return 0;
}
