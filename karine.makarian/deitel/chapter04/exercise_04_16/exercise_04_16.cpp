#include <iostream>

int
main()
{
    int time;

    while (true) {
        std::cout << "Enter hours worked (-1 to exit): ";
        std::cin >> time;
    
        if (-1 == time) {
            break;
        }
        
        if (time < 0) {
            std::cerr << "Error 1: Work time can't be negative." << std::endl;
            return 1;
        }

        double money;

        std::cout << "Enter your hourly rate ($00.00): $";
        std::cin >> money;

        if (money < 0) {
            std::cerr << "Error 2: Hourly rate can't be negative." << std::endl;
            return 2; 
        }

        double salary = money * time;
    
        if (time > 40) {
            salary *= 1.5;
            salary -= money * 20;
        }

        std::cout << "Salary: $" << salary << std::endl;
    }

    return 0;
}

