#include "Invoice.hpp"
#include <iostream>
#include <string>

int
main()
{
    std::string number = "00125";
    std::string description = ""; 
    int quantity = 10;
    int pricePerItem = 500;

    Invoice item(number, description, quantity, pricePerItem);
    std::cout << "Item's number is " << item.getNumber() << std::endl;
    std::cout << "Item's price is " << item.getPricePerItem() << std::endl;
    quantity = -10;
    item.setQuantity(quantity);
    std::cout << "Item's quantity remained after purchase is " << item.getQuantity() << std::endl;
    quantity = 25;
    item.setQuantity(quantity);
    std::cout << "Item's amount is " << item.getInvoiceAmount() << std::endl;

    return 0;
}

